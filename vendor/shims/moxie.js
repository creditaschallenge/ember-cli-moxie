(function() {
  function vendorModule() {
    'use strict';

    return { 'default': self['mOxie'] };
  }

  define('moxie', [], vendorModule);
})();
