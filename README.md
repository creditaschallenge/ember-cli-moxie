# Ember-cli-moxie

Shallow wrapper around [the moxie upload library](https://github.com/moxiecode/moxie). For now it just helps with the building the library and importing it on the app, but it might grow to include lightweight components and other utilities to help 'emberize' moxie's interfaces.

## Usage

On `ember-cli-build.js`:
```js
var app = new EmberApp(defaults, {
  'ember-cli-moxie': {
    // Check moxie's README for the valid runtimes and components
    runtimes: ['html5', 'html4', 'flash'],
    components: ['xhr/XMLHttpRequest', 'file/FileInput', 'file/FileDrop'],
  },
});
```

And then, on (for example) a component:
```js
import m0xie from 'moxie';

export default Ember.Component.extend({
  setupFileInput: Ember.on('didInsertElement', function() {
    var input = new m0xie.FileInput();
    /* ... */
  }),
});
```

## Installation

* `git clone` this repository
* `npm install`
* `bower install`

## Running

* `ember server`
* Visit your app at http://localhost:4200.

## Running Tests

* `npm test` (Runs `ember try:testall` to test your addon against multiple Ember versions)
* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://ember-cli.com/](http://ember-cli.com/).
