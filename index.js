/* jshint node: true */
'use strict';

var MoxieBuilder = require('./build-support/moxie-builder');
var mergeTrees = require('broccoli-merge-trees');
var Funnel = require('broccoli-funnel');

module.exports = {
  name: 'ember-cli-moxie',

  included: function(app, parentAddon) {
    this._super.included.apply(this, arguments);

    var appConfig = (app || parentAddon).options || {};
    this._config = appConfig[this.name];

    app.import({
      development: 'vendor/moxie/js/moxie.js',
      production: 'vendor/moxie/js/moxie.min.js',
    });

    app.import('vendor/shims/moxie.js');
  },

  treeForVendor: function() {
    var tree = this._super.treeForVendor.apply(this, arguments);
    var moxie = new MoxieBuilder('node_modules/moxie', this._config);

    return mergeTrees([tree, moxie]);
  },

  treeForPublic: function() {
    var tree = this._super.treeForPublic.apply(this, arguments);

    var moxieAssets = new Funnel('./node_modules/moxie/bin/', {
      include: ['flash/*', 'silverlight/*'],
      destDir: '/assets/vendor/moxie/',
    });

    if (tree == null) {
      return moxieAssets;
    }

    return mergeTrees([tree, moxieAssets]);
  },
};
