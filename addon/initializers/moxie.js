import mOxie from 'moxie';

export default {
  name: 'mOxie',

  initialize() {
    mOxie.Env.swf_url = '/assets/vendor/moxie/flash/Moxie.min.swf';
    mOxie.Env.xap_url = '/assets/vendor/moxie/silverlight/Moxie.min.xap';
  },
};
