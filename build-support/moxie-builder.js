var Plugin = require('broccoli-plugin');
var rimraf = require('rimraf');
var path = require('path');
var fs = require('fs');
var childProcess = require('child_process');
var symlinkOrCopy = require('symlink-or-copy').sync;

MoxieBuilder.prototype = Object.create(Plugin.prototype);
MoxieBuilder.prototype.constructor = MoxieBuilder;

function MoxieBuilder(sourcePath, options) {
  options = options || {};

  Plugin.call(this, [], {
    annotation: options.annotation,
  });

  this.sourcePath = sourcePath;
  this.components = options.components || [];
  this.runtimes   = options.runtimes   || [];
}

MoxieBuilder.prototype.build = function() {
  var self = this;

  if (this._isCacheFilled()) {
    this._copyFromCache();
    return;
  }

  return this._npmInstall()
    .then(() => this._linkBinToCache())
    .then(() => this._runJake())
    .then(() => this._copyFromCache());
};

MoxieBuilder.prototype._isCacheFilled = function() {
  var target = path.join(this.cachePath, 'js');

  return fs.existsSync(path.join(target, 'moxie.js')) &&
         fs.existsSync(path.join(target, 'moxie.min.js'));
};

MoxieBuilder.prototype._linkBinToCache = function() {
  var buildPath = path.join(this.sourcePath, 'bin', 'js');
  var target    = path.join(this.cachePath, 'js');

  fs.symlinkSync(path.resolve(buildPath), target);
};

MoxieBuilder.prototype._copyFromCache = function() {
  symlinkOrCopy(
    this.cachePath,
    path.join(this.outputPath, 'moxie')
  );
};

MoxieBuilder.prototype._npmInstall = function() {
  var sourcePath = this.sourcePath;

  return new Promise(function(resolve, reject) {
    childProcess.exec('npm install', {
      stdio: [0, 'pipe', 'pipe'],
      cwd: sourcePath,
    }, function(err) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

MoxieBuilder.prototype._removePrevBuild = function() {
  var targetPath = path.join(this.sourcePath, 'bin');

  return new Promise(function(resolve, reject) {
    rimraf(targetPath, function(err) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};


MoxieBuilder.prototype._runJake = function() {
  var sourcePath = this.sourcePath;
  var self = this;

  return new Promise(function(resolve, reject) {
    childProcess.execFile('node_modules/.bin/jake', self._jakeOptions(), {
      cwd: sourcePath,
      stdio: [0, 'pipe', 'pipe']
    }, function(err) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

MoxieBuilder.prototype._jakeOptions = function() {
  var task = "mkjs";
  var runtimesOption = 'runtimes=';

  if (this.components.length > 0) {
    task += '[' + this.components.join(',') + ']';
  }

  if (this.runtimes.length > 0) {
    runtimesOption += this.runtimes.join(',');
  } else {
    runtimesOption += 'all';
  }

  return [task, runtimesOption];
};

module.exports = MoxieBuilder;
